using System;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace babbys_first_monogame
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private SpriteFont _debugFont;

        private Level _level;
        private Model _map_model;

        private Player _player;
        private Texture2D _playerTex;

        private Camera _camera;

        private Matrix _projection;
        private Matrix _view;
        private Matrix _world = Matrix.Identity;

        private Polymap _polymap;

        private String DebugPrintMatrix(Matrix m)
        {
            StringBuilder sb = new StringBuilder("", 500);

            String a = String.Format("{0, -12} {1, -12} {2, -12} {3, -12}\n", m.M11, m.M12, m.M13, m.M14);
            sb.Append(a);

            a = String.Format("{0, -12} {1, -12} {2, -12} {3, -12}\n", m.M21, m.M22, m.M23, m.M24);
            sb.Append(a);

            a = String.Format("{0, -12} {1, -12} {2, -12} {3, -12}\n", m.M31, m.M32, m.M33, m.M34);
            sb.Append(a);

            a = String.Format("{0, -12} {1, -12} {2, -12} {3, -12}\n", m.M41, m.M42, m.M43, m.M44);
            sb.Append(a);

            return sb.ToString();
        }
        private void _graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.MultiSampleCount = 8;
        }

        public Texture2D CreateRectangle(int X, int Y, Color color)
        {
            Texture2D rect = new Texture2D(GraphicsDevice, X, Y);
            Color[] data = new Color[X * Y];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = color;
            }
            rect.SetData(data);
            return rect;
        }

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                GraphicsProfile = GraphicsProfile.HiDef,
                PreferredBackBufferWidth = 1680,
                PreferredBackBufferHeight = 720,
                //IsFullScreen = true,
                PreferMultiSampling = true,
                SynchronizeWithVerticalRetrace = false
            };
            _graphics.PreparingDeviceSettings += _graphics_PreparingDeviceSettings;
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(1000f/240f);
            //IsFixedTimeStep = false;
            //_graphics.SynchronizeWithVerticalRetrace = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _debugFont = Content.Load<SpriteFont>("debug");

            _map_model = Content.Load<Model>("maps/helloworld/polymap");

            _polymap = new Polymap(_map_model);

            _level = new Level(new Vector2(128, 128), _polymap);

            _player = new Player(new Vector2(2730, 2200), new Vector2(16, 24), _level);
            _camera = new Camera(_player.P, _graphics);

            _playerTex = Content.Load<Texture2D>("player");


            //_projection = Matrix.CreatePerspectiveFieldOfView(
            //    MathHelper.ToRadians(45f),
            //    _graphics.GraphicsDevice.Viewport.AspectRatio,
            //    0.1f,
            //    100f
            //);
            _projection = Matrix.CreateOrthographic(_graphics.GraphicsDevice.Viewport.AspectRatio * 720 / 48, 720 / 48, 0.1f, 100f);
            _view = Matrix.CreateLookAt(
                new Vector3(0, 0, 1),
                new Vector3(0, 0, 0),
                new Vector3(0, 1, 0)
            );
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            _player.Update(gameTime);

            _camera.SetTargetPosition(_player.P);
            _camera.Update(gameTime);
            _view.M41 = (-720 * _graphics.GraphicsDevice.Viewport.AspectRatio / 2 + _camera.Transform.M41) / 48;
            _view.M42 = (720 / 2 - _camera.Transform.M42) / 48;

            base.Update(gameTime);

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGray);

            foreach (ModelMesh mesh in _map_model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.1f, 0.1f, 0.1f);
                    effect.World = _world;
                    effect.View = _view;
                    effect.Projection = _projection;
                }
                mesh.Draw();
            }


            // TODO: Add your drawing code here
            _spriteBatch.Begin(transformMatrix: _camera.Transform, samplerState: SamplerState.LinearClamp);
            _spriteBatch.Draw(
                _playerTex,
                _player.P,
                null,
                Color.White,
                0f,
                _player.GetHalfSize(),
                Vector2.One,
                SpriteEffects.None,
                0f
            );
            _spriteBatch.End();

            // debug info / hud etc.
            _spriteBatch.Begin(samplerState: SamplerState.LinearClamp);
            _spriteBatch.DrawString(
                _debugFont,
                _player.ToString(),
                new Vector2(8, 8),
                Color.Black
            );
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
