﻿using System;
using Microsoft.Xna.Framework;

namespace babbys_first_monogame
{
    public class AABoundingBox
    {
        public Vector2 P { get; set; } = new Vector2(0, 0);
        protected Vector2 _s = new Vector2(0, 0);


        public AABoundingBox()
        {

        }
        public AABoundingBox(Vector2 pos, Vector2 halfSize)
        {
            P = pos;
            _s = halfSize;
        }

        public Vector2 GetSize()
        {
            return _s * 2;
        }

        public Vector2 GetHalfSize()
        {
            return _s;
        }

        protected float TestAxis(float p1, float p2, float s1, float s2)
        {
            float delta;

            if (p1 < p2)
            {
                delta = (p1 + s1) - (p2 - s2);
                if (delta < 0f)
                {
                    return 0;
                }
            }
            else
            {
                delta = (p1 - s1) - (p2 + s2);
                if (delta > 0f)
                {
                    return 0;
                }
            }
            return delta;
        }

        public Vector2 TestCollision(AABoundingBox box)
        {
            var deltaX = TestAxis(P.X, box.P.X, _s.X, box._s.X);
            if (deltaX == 0)
            {
                return new Vector2(0, 0);
            }
            var deltaY = TestAxis(P.Y, box.P.Y, _s.Y, box._s.Y);
            if (deltaY == 0)
            {
                return new Vector2(0, 0);
            }

            if (Math.Abs(deltaX) >= Math.Abs(deltaY))
            {
                return new Vector2(0, deltaY);
            }
            else
            {
                return new Vector2(deltaX, 0);
            }
        }

        public Vector2 TestCollision(Slope slope)
        {
            var deltaX = TestAxis(P.X, slope.P.X, _s.X, slope._s.X);
            if (deltaX == 0)
            {
                return new Vector2(0, 0);
            }
            var deltaY = TestAxis(P.Y, slope.P.Y, _s.Y, slope._s.Y);
            if (deltaY == 0)
            {
                return new Vector2(0, 0);
            }

            Vector2 corner = new Vector2(0, 0);
            if (slope.N.X > 0)
            {
                corner.X = P.X - _s.X;
            }
            else
            {
                corner.X = P.X + _s.X;
            }
            if (slope.N.Y > 0)
            {
                corner.Y = P.Y - _s.Y;
            }
            else
            {
                corner.Y = P.Y + _s.Y;
            }

            float projectedCorner = slope.N.X * corner.X + slope.N.Y * corner.Y;
            float projectedSlope = slope.N.X * slope.P.X + slope.N.Y * slope.P.Y;

            float deltaSlope = projectedCorner - projectedSlope;

            if (deltaSlope > 0)
            {
                return new Vector2(0, 0);
            }

            if (Math.Abs(deltaSlope) < Math.Abs(deltaX) && Math.Abs(deltaSlope) < Math.Abs(deltaY))
            {
                return slope.N * deltaSlope;
            }
            else if (Math.Abs(deltaX) < Math.Abs(deltaY))
            {
                return new Vector2(deltaX, 0);
            }
            else
            {
                return new Vector2(0, deltaY);
            }
        }

        public Vector2 TestCollision(Triangle triangle)
        {
            var vectors = new Vector2[5];

            var deltaX = TestAxis(P.X, triangle.P.X, _s.X, triangle.S.X);
            if (deltaX == 0)
            {
                return new Vector2(0, 0);
            }
            vectors[0] = new Vector2(deltaX, 0);

            var deltaY = TestAxis(P.Y, triangle.P.Y, _s.Y, triangle.S.Y);
            if (deltaY == 0)
            {
                return new Vector2(0, 0);
            }
            vectors[1] = new Vector2(0, deltaY);

            for (int i = 0; i < 3; ++i)
            {
                var normal = triangle.N[i];
                var vertex = triangle.V[i];
                var corner = GetCorner(normal);

                float projectedCorner = Vector2.Dot(normal, corner);
                float projectedVertex = Vector2.Dot(normal, vertex);

                float delta = projectedCorner - projectedVertex;

                if (delta > 0)
                {
                    return new Vector2(0, 0);
                }

                vectors[i + 2] = delta * normal;
            }

            var shortest = vectors[0];

            foreach (var v in vectors)
            {
                if (shortest.Length() > v.Length())
                    shortest = v;
            }

            return shortest;
        }

        private Vector2 GetCorner(Vector2 normal)
        {
            Vector2 corner = new Vector2(0, 0);
            if (normal.X > 0)
            {
                corner.X = P.X - _s.X;
            }
            else
            {
                corner.X = P.X + _s.X;
            }
            if (normal.Y > 0)
            {
                corner.Y = P.Y - _s.Y;
            }
            else
            {
                corner.Y = P.Y + _s.Y;
            }

            return corner;
        }
    }

    public class Slope : AABoundingBox
    {
        public Vector2 N { get; private set; }

        public Slope(Vector2 pos, Vector2 halfSize, Vector2 normal)
        {
            P = pos;
            _s = halfSize;
            N = normal;
        }
    }

    public class Triangle
    {
        public Vector2 P { get; }
        public Vector2 S { get; }
        public Vector2[] V { get; } = new Vector2[3];
        public Vector2[] N { get; } = new Vector2[3];

        public Vector2 maxV { get; private set; }
        public Vector2 minV { get; private set; }

        // creates a triangle assuming clockwise winding
        public Triangle(Vector2 v1, Vector2 v2, Vector2 v3)
        {
            V[0] = v1;
            V[1] = v2;
            V[2] = v3;

            for (int i1 = 0; i1 < 3; ++i1)
            {
                int i2 = (i1 + 1) % 3;
                Vector2 delta = V[i2] - V[i1];
                N[i1].X = delta.Y;
                N[i1].Y = -delta.X;
                N[i1].Normalize();
            }

            SetExtrema();
            P = minV + (maxV - minV) / 2;
            S = (maxV - minV) / 2;
        }

        public Triangle(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 n1, Vector2 n2, Vector2 n3)
        {
            V[0] = v1;
            V[1] = v2;
            V[2] = v3;

            N[0] = n1;
            N[1] = n2;
            N[2] = n3;

            P = V[0] + V[1] + V[2] / 3;

            SetExtrema();
            P = minV + (maxV - minV) / 2;
            S = (maxV - minV) / 2;
        }

        private void SetExtrema()
        {
            float maxX = V[0].X;
            float maxY = V[0].Y;
            float minX = V[0].X;
            float minY = V[0].Y;

            for (int i = 1; i < 3; ++i)
            {
                var v = V[i];

                if (v.X > maxX)
                    maxX = v.X;
                else if (v.X < minX)
                    minX = v.X;

                if (v.Y > maxY)
                    maxY = v.Y;
                else if (v.Y < minY)
                    minY = v.Y;
            }

            minV = new Vector2(minX, minY);
            maxV = new Vector2(maxX, maxY);
        }
    }
}
