﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace babbys_first_monogame
{
    public class Level
    {
        public Vector2 StartPosition { get; }
        public uint Width { get; } = 256;
        public uint Height { get; } = 256;
        public uint GridWidth { get; } = 48;
        public uint GridHeight { get; } = 48;

        public Polymap Polymap;

        public List<Triangle>[,] Polygrid { get; } 
        public Level(Vector2 startPosition, Polymap polymap)
        {
            StartPosition = startPosition;
            Polymap = polymap;

            Polygrid = new List<Triangle>[Width, Height];

            for (uint x = 0; x < Width; ++x)
            {
                for (uint y = 0; y < Height; ++y)
                {
                    Polygrid[x, y] = new List<Triangle>(10);
                }
            }

            foreach (var tri in Polymap.Triangles)
            {
                uint minX = (uint)Math.Floor(tri.minV.X / 48);
                uint minY = (uint)Math.Floor(tri.minV.Y / 48);

                uint maxX = (uint)Math.Ceiling(tri.maxV.X / 48);
                uint maxY = (uint)Math.Ceiling(tri.maxV.Y / 48);

                for (uint x = minX; x < maxX; ++x)
                {
                    for (uint y = minY; y < maxY; ++y)
                    {
                        var boxpos = new Vector2(x * GridWidth + GridWidth / 2, y * GridHeight + GridHeight / 2);
                        var boxsize = new Vector2(GridWidth / 2, GridHeight / 2);
                        var tile = new AABoundingBox(boxpos, boxsize);

                        var result = tile.TestCollision(tri);
                        if (result != new Vector2(0, 0))
                        {
                            Polygrid[x, y].Add(tri);
                        }
                    }
                }
            }
        }
    }
}
