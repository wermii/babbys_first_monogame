﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace babbys_first_monogame
{
    public class Player : AABoundingBox
    {
        private Level _level;
        private Vector2 _velocity = new Vector2(0, 0);
        private Vector2 _velocityTerminal = new Vector2(1000, 1100);
        private float _velocityMaxGrounded = 300f;
        private float _velocityMaxAir = 380f;
        public float Gravity { get; private set; }
        private float _coyote = 0;
        private bool _isGrounded = false;
        private bool _isJumpPending = false;
        private int _nearbyTriangleCount = 0;

        private float _movementDirX = 0;
        private float _movementDirY = 0;
        private bool _isJumpPressed = false;
        private bool _isJumpHeld = false;

        private const float _jumpVelocity = 500f;
        private const float _velocityToJumpRate = 0.15f;
        private const float _gravityNormal = 700f;
        private const float _gravityCancelledJump = 1800f;
        private const float _gravityAdditional = 600f;
        private const float _jumpCancelThreshold = -150f;
        private const float _accelerationGrounded = 0.3f;
        private const float _accelerationAir = 0.3f;
        private const float _decelerationGrounded = 0.9f;
        private const float _decelerationAir = 0.5f;
        private const float _decelerationGroundedAlt = 0.1f;
        private const float _coyoteAmount = 0.150f;

        public Vector2 Velocity
        {
            get { return _velocity; }
        }

        public Player(Vector2 position, Vector2 halfSize, Level level)
        {
            P = position;
            _s = halfSize;
            _level = level;
        }

        private void SetGrounded()
        {
            _isGrounded = true;
            _coyote = _coyoteAmount;
        }

        private HashSet<Triangle> GetNearbyTriangles()
        {
            uint width = _level.Width;
            uint height = _level.Height;

            uint gridWidth = _level.GridWidth;
            uint gridHeight = _level.GridHeight;

            var min = P - _s;
            var max = P + _s;

            uint minX = Math.Clamp((uint)Math.Floor(min.X / gridWidth), 0, width);
            uint minY = Math.Clamp((uint)Math.Floor(min.Y / gridWidth), 0, height);

            uint maxX = Math.Clamp((uint)Math.Ceiling(max.X / gridWidth), 0, width);
            uint maxY = Math.Clamp((uint)Math.Ceiling(max.Y / gridWidth), 0, height);

            var nearby = new HashSet<Triangle>();

            for (uint x = minX; x < maxX; ++x)
            {
                for (uint y = minY; y < maxY; ++y)
                {
                    var cell = _level.Polygrid[x, y];

                    foreach (Triangle t in cell)
                    {
                        nearby.Add(t);
                    }
                }
            }

            _nearbyTriangleCount = nearby.Count;

            return nearby;
        }

        private void EvaluatePolyCollisions()
        {
            var nearby = GetNearbyTriangles();

            foreach (var t in nearby)
            {
                Vector2 result = TestCollision(t);

                if (result != new Vector2(0, 0))
                {
                    P -= result;
                    var normalResult = Vector2.Normalize(result);
                    var normalVelocity = Vector2.Normalize(_velocity);

                    if (Vector2.Dot(normalResult, normalVelocity) > 0)
                    {
                        Vector2 bounce = Vector2.Reflect(_velocity, normalResult);
                        bounce = Vector2.Lerp(bounce, _velocity, 0.5f);
                        _velocity = bounce;
                    }

                    if (normalResult.Y > 0.25)
                    {
                        SetGrounded();
                    }
                }
            }
        }
        private void PollInput()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                _movementDirX = -1;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                _movementDirX = 1;
            }
            else
            {
                _movementDirX = 0;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                _movementDirY = 1;
            }
            else
            {
                _movementDirY = 0;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                _isJumpPressed = !_isJumpHeld;
                _isJumpHeld = true;
            }
            else
            {
                _isJumpHeld = false;
                _isJumpPressed = false;
            }
        }

        private void ApplyMovement(float deltaTime)
        {
            float acceleration;
            float deceleration;
            float velocityMax;

            if (_isGrounded)
            {
                acceleration = _accelerationGrounded;
                deceleration = _decelerationGrounded;
                velocityMax = _velocityMaxGrounded;
            }
            else
            {
                acceleration = _accelerationAir;
                deceleration = _decelerationAir;
                velocityMax = _velocityMaxAir;
            }

            float velocityTarget = velocityMax * _movementDirX;
            bool isDecelerating;

            if (_movementDirX != 0)
            {
                isDecelerating = false;
            }
            else
            {
                isDecelerating = true;
            }

            if (isDecelerating)
            {
                _velocity.X = MathHelper.Lerp(velocityTarget, _velocity.X, (float)Math.Pow(1f - deceleration, deltaTime * 10f));
            }
            else if (_velocity.X * _movementDirX < velocityTarget * _movementDirX)
            {
                _velocity.X = MathHelper.Lerp(velocityTarget, _velocity.X, (float)Math.Pow(1f - acceleration, deltaTime * 10f));
            }
            else if (_isGrounded)
            {
                _velocity.X = MathHelper.Lerp(velocityTarget, _velocity.X, (float)Math.Pow(1f - _decelerationGroundedAlt, deltaTime * 10f));
            }

            if ((_velocity.Y > 0 || _isGrounded) && _isJumpPressed)
            {
                _isJumpPending = true;
            }

            if (_isJumpPending && _isGrounded && _isJumpHeld)
            {
                _velocity.Y -= _jumpVelocity + Math.Abs(_velocity.X) * _velocityToJumpRate;
                _isGrounded = false;
                _isJumpPending = false;
            }

            Gravity = _gravityNormal + _gravityAdditional * _movementDirY;

            if (_isJumpHeld)
            {
                Gravity = _gravityNormal;
            }
            else if (_velocity.Y < _jumpCancelThreshold)
            {
                Gravity = _gravityCancelledJump;
            }

            if (_coyote <= 0)
            {
                _isGrounded = false;
            }
            else
            {
                _coyote -= deltaTime;
            }
        }

        private void ApplyPhysics(float deltaTime)
        {
            _velocity.Y += Gravity * deltaTime;

            _velocity = Vector2.Clamp(_velocity, -_velocityTerminal, _velocityTerminal);

            P += _velocity * deltaTime;
        }

        public void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // test movement
            PollInput();
            ApplyMovement(deltaTime);
            ApplyPhysics(deltaTime);
            EvaluatePolyCollisions();
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder("", 500);

            sb.AppendLine("X:  " + P.X);
            sb.AppendLine("Y:  " + P.Y);
            sb.AppendLine("vX: " + Velocity.X);
            sb.AppendLine("vY: " + Velocity.Y);
            sb.AppendLine("G:  " + Gravity);

            sb.AppendLine("\nnearby triangles: " + _nearbyTriangleCount);

            return sb.ToString();
        }
    }
}
