using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace babbys_first_monogame
{
    public class Polymap
    {
        public List<Triangle> Triangles = new List<Triangle>();

        public Polymap(Model model)
        {
            foreach (var mesh in model.Meshes)
            {
                foreach (var part in mesh.MeshParts)
                {
                    VertexPositionNormalTexture[] vertex = new VertexPositionNormalTexture[part.VertexBuffer.VertexCount];
                    part.VertexBuffer.GetData<VertexPositionNormalTexture>(vertex);

                    var icount = part.IndexBuffer.IndexCount / 3;
                    short[] index = new short[icount * 3];
                    part.IndexBuffer.GetData<short>(index);

                    for (int i = 0; i < icount; ++i)
                    {
                        var i1 = index[i * 3];
                        var i2 = index[i * 3 + 1];
                        var i3 = index[i * 3 + 2];

                        var v1 = new Vector2(vertex[i1].Position.X * 48, vertex[i1].Position.Y * -48);
                        var v2 = new Vector2(vertex[i2].Position.X * 48, vertex[i2].Position.Y * -48);
                        var v3 = new Vector2(vertex[i3].Position.X * 48, vertex[i3].Position.Y * -48);

                        var t = new Triangle(v1, v2, v3);
                        Triangles.Add(t);
                    }
                }
            }
        }
    }
}