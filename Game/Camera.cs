﻿using System;
using Microsoft.Xna.Framework;

namespace babbys_first_monogame
{
    public class Camera
    {
        private GraphicsDeviceManager _graphics;
        private Matrix _targetTransform;
        private float _virtualHeight = 720;
        public Matrix Transform { get; private set; }
        public float Zoom
        {
            get { return _targetTransform.M44; }
            private set { _targetTransform.M44 = value; }
        }

        private float movementRate = 0.7f;

        public Camera(Vector2 position, GraphicsDeviceManager graphicsDevice)
        {
            _graphics = graphicsDevice;

            _targetTransform = Matrix.CreateTranslation(new Vector3(0, 0, 0));
            SetTargetPosition(position);
            Zoom = _virtualHeight / _graphics.PreferredBackBufferHeight;

            Transform = _targetTransform;
        }

        public void SetTargetPosition(Vector2 position)
        {
            _targetTransform.M41 = -position.X + _graphics.PreferredBackBufferWidth * Zoom / 2;
            _targetTransform.M42 = -position.Y + _graphics.PreferredBackBufferHeight * Zoom / 2;
        }

        public void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Transform = Matrix.Lerp(_targetTransform, Transform, (float)Math.Pow(movementRate, deltaTime * 60f));
        }
    }
}
